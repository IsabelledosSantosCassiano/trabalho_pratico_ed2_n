import argparse
import os
import re
import math
from collections import Counter

# Função para contar palavras em um arquivo:
def count_words_in_file(file_path):
    word_count = Counter()
    with open(file_path, 'r') as file:
        text = file.read().lower()
        words = re.findall(r'\b\w+\b', text)
        words = [word for word in words if len(word) >= 2]
        word_count.update(words)
    return word_count

# Função para calcular o TF-IDF de um termo em um documento:
def tfidf(term, word_count, document_index):
    tf = word_count[term] / sum(word_count.values())
    idf = math.log(len(document_index) / (1 + sum(1 for doc in document_index.values() if term in doc)))
    return tf * idf

# Função para indexar um documento:
def index_document(file_path, document_index):
    word_count = count_words_in_file(file_path)
    document_index[file_path] = word_count

# Função para buscar documentos relevantes para um termo de busca:
def search(term, document_index):
    results = {}
    for document, word_count in document_index.items():
        results[document] = tfidf(term, word_count, document_index)
    sorted_results = dict(sorted(results.items(), key=lambda item: item[1], reverse=True))
    return sorted_results

def main():
    parser = argparse.ArgumentParser(description='Processa comandos para analisar arquivos.')
    parser.add_argument('--freq', type=int, help='Exibe as N palavras mais frequentes em um arquivo')
    parser.add_argument('--freq-word', help='Exibe a contagem de uma palavra específica em um arquivo')
    parser.add_argument('--search', help='Exibe documentos relevantes para um termo de busca')
    parser.add_argument('arquivos', nargs='*', help='Nomes dos arquivos para análise')

    args = parser.parse_args()

    document_index = {}
    
    for arquivo in args.arquivos:
        index_document(arquivo, document_index)

    if args.freq:
        if len(args.arquivos) == 1:
            document = args.arquivos[0]
            if document in document_index:
                word_count = document_index[document]
                most_common = word_count.most_common(args.freq)
                for word, count in most_common:
                    print(f"{word}: {count}")
            else:
                print(f"Arquivo '{document}' não encontrado.")
    #else:
        #print("\nForneça exatamente um arquivo para a operação.")

    if args.freq_word:
        word = args.freq_word
        if word:
            for document, word_count in document_index.items():
                count = word_count.get(word, 0)
                print(f"A palavra '{word}' aparece {count} vezes em {document}")
        else:
            print("O argumento '--freq-word' deve ser fornecido com a palavra a ser contada.")

    if args.search:
        term = args.search
        search_results = search(term, document_index)
        print(f"Documentos mais relevantes para '{term}':")
        for document, relevance in search_results.items():
            print(f"{document}: TF-IDF = {relevance}")

if __name__ == "__main__":
    main()

