# Trabalho prático EDII - 2023/2

O trabalho solicitado na disciplina de Estrutura de Dados II do curso de Análise e Desenvolvimento de Sistemas da Universidade Federal do Paraná, consiste em um programa capaz de indexar palavras de um ou mais documentos de texto. Tal programa é chamado `indexer`.<br><br>

## Equipe
```
Emanuelly Wolski GRR20211614, Emilly Wolski GRR, e Isabelle dos Santos Cassiano GRR
```
<br>

## Ambiente e Linguagem
* Desenvolvido utilizando o Sistema Operacional Windows;
* IDE: Visual Studio Code;
* Linguagem Pyhton, versão: 3.11.1<br><br>

## Instalação do Python
* Para instalar o Python no **Windows**, acesse o site oficial da linguagem Python, e siga para a área de Downloads. Um botão com a versão mais recente aparecerá na tela. É necessário escolher a opção mais adequada ao seu ambiente. Site: `https://www.python.org/downloads/`

* Após o download do instalador, execute-o. Na primeira tela, certifique-se de marcar a opção “Add Python to PATH” e clique em “Install Now”.

* O instalador irá configurar o Python em seu computador. Aguarde até que a instalação seja concluída.

* Após a conclusão da instalação, você pode verificar se o Python foi instalado corretamente abrindo o Prompt de Comando e digitando “python –version”. Se a versão do Python for exibida, significa que a instalação foi bem-sucedida.<br><br>

* No **Linux**, o Python geralmente já está instalado. No entanto, você pode verificar se está instalado digitando "python --version" ou "python3 --version". Se não estiver instalado, você pode instalá-lo usando o gerenciador de pacotes específico para a sua distribuição Linux. Por exemplo, no Ubuntu, você pode usar o seguinte comando: "sudo apt-get install python3".<br><br>

Indica-se a utilização do **Visual Studio Code como IDE**. Acesse o link 1 e confira o passo a passo em vídeo. Se utilizar outra IDE, certifique-se que o python esteja funcionando nesse ambiente.
<br><br>


Essas orientações foram baseadas nos conteúdos das fontes abaixo, caso tenha alguma dificuldade, pode consultá-las para garantir que a instalação funcione corretamente. <br>
* Link 1<br>Python no VS Code - Windows:<br>https://www.youtube.com/watch?v=7Kpd6eprz4k

* Link 2:<br>Instalação Python no Windows e Linux:<br>
https://awari.com.br/como-instalar-python-guia-passo-a-passo-para-iniciantes/?utm_source=blog&utm_campaign=projeto+blog&utm_medium=Como%20Instalar%20Python:%20Guia%20Passo%20a%20Passo%20para%20Iniciantes#:~:text=um%20desempenho%20adequado.-,Passo%20a%20passo%20para%20instalar%20Python%20no%20Windows,do%20instalador%2C%20execute%2Do.

<br>

## Arquivos para testes

Com o ambiente funcionanedo corretamente, você pode testar o programa com alguns arquivos que estão disponíveis no link a seguir. Baixe os que preferir para testar o programa. `http://200.236.3.126:8999/ds143-texts/`<br><br>


## Funcionamento

```

COMANDOS PARA EXECUÇÃO DAS FUNÇÕES ESPECÍFICAS DO PROGRAMA:

    1) python indexer --freq N ARQUIVO
    2) python indexer --freq-word PALAVRA ARQUIVO
    3) python indexer --search TERMO ARQUIVO [ARQUIVO ...]


IMPORTANTE
    O programa **indexer** transforma todas as letras para minúsculas 
    e ignora caracteres como números e pontuações.


OPÇÕES
1)
  python indexer --freq N ARQUIVO

  Exemplo: 
  python indexer.py --freq 10 103.txt 

  Exibe o número de ocorrência das N palavras que mais aparecem 
  em ARQUIVO, em ordem decrescente de ocorrência.

2)
  python indexer --freq-word PALAVRA ARQUIVO

  Exemplo:
  python indexer.py --freq-word how 101.txt  

  Exibe o número de ocorrências de PALAVRA em ARQUIVO. 

3)
  python indexer --search TERMO ARQUIVO [ARQUIVO ...]

  Exemplo:
  python indexer.py --search and .\101.txt .\103.txt  

  Exibe uma listagem dos ARQUIVOS mais relevantes 
  encontrados pela busca por TERMO. A listagem é apresentada 
  em ordem descrescente de relevância. Para tanto, o 
  programa utiliza o cálculo TF-IDF (Term Frequency-
  Inverse Document Frequency).
    
  TERMO pode conter mais de uma palavra. Neste caso, deve ser 
  indicado entre aspas.
```

<br><br>
## Requisitos Seguidos

O programa deve ser capaz de ser compilado, se for o caso, e executado em um ambiente Linux. 

O funcionamento da aplicação **indexer** tem as seguintes peculiaridades:

- Transformar todas os caracteres em minúsculo, ou seja, comportamento de *ignore-case*;
- Ignorar palavras com menos de 2 caracteres;
- Ignorar caracteres que não sejam letras, como números e pontuações:
  Por conta disso, palavras compostas como `bem-vindo` serão separadas em duas, `bem` e `vindo`.<br><br><br>



## Cálculo de Relevância

O cálculo para determinar a relevância de um texto para um dado termo de busca foi realizado utilizando a técnica **Term Frequency-Inverse Document Frequency** (https://pt.wikipedia.org/wiki/Tf–idf e https://en.wikipedia.org/wiki/Tf–idf#Term_frequency).<br><br>

**Term Frequency (TF)** ou Frequência de um termo `t`, foi calculada como o número de vezes que `t` aparece no documento, dividido pelo número total de palavras no documento:

```
TF(t,d) = (Número de vezes que t aparece em d) / (Total de palavras em d)
```

**Inverse Document Frequency (IDF)** ou Frequência inversa de documento para um conjunto de documentos `D` é o logaritmo da divisão do número total de documentos pelo número de documentos que contém o termo `t` em questão:

```
IDF(t,D) = log[ (Número de Documentos) / (Número de documentos em que t está presente) ]
```

Logaritmo de base 10.

Por fim, TFIDF é a multiplicação das duas medidas. Ela indica qual a relevância de um termo `t` para um documento `d` dentre uma coleção de documentos `D`:

```
TFIDF(t,d,D) = TF(t,d) * IDF(t,D)
```

Para termos com mais de uma palavra, deve ser calculado a média entre o TF-IDF de cada palavra.

Mais uma referência: http://www.tfidf.com
